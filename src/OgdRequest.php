<?php

namespace Drupal\ogd;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use GuzzleHttp\Psr7\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for nodes of different types.
 */
class OgdRequest implements ContainerFactoryPluginInterface {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * Constructs a DefaultFetcher object.
   *
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   A Guzzle client object.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   A config factory for retrieving required config objects.
   */
  public function __construct(ClientFactory $http_client_factory, ConfigFactoryInterface $config_factory) {
    $this->httpClientFactory = $http_client_factory;
    $this->config = $config_factory->get('ogd.configsettings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $container->get('http_client_factory')
    );
  }

  /**
   * OGD API Call.
   *
   * @param string $call
   *   Pass OGD API method name.
   * @param array $param
   *   Request Input Parameter.
   *
   * @return json
   *   Reurn data in JSON Format.
   */
  public function request($call, array $param) {

    $api_url = $this->config->get('ogd_base_url');
    $api_key = $this->config->get('ogd_api_key');

    $client = $this->httpClientFactory->fromOptions([
      'base_uri' => $api_url,
    ]);

    $param['api-key'] = $api_key;
    $param['format'] = 'json';

    $response = $client->get($call, [
      'query' => $param,
    ]);

    return Json::decode($response->getBody()->getContents());
  }

}
