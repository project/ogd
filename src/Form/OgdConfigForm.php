<?php

namespace Drupal\ogd\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides Cnfiguration Form for OGD Module.
 *
 * @internal
 */
class OgdConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ogd.configsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ogd_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ogd.configsettings');

    $form['ogd'] = [
      '#type' => 'details',
      '#title' => $this->t('OGD API settings'),
      '#open' => TRUE,
    ];
    $form['ogd']['ogd_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OGD API Key'),
      '#required' => TRUE,
      '#default_value' => $config->get('ogd_api_key'),
    ];
    $ogdBaseUrl = $config->get('ogd_base_url');
    if (!isset($ogdBaseUrl)) {
      // Providing Default Value.
      $ogdBaseUrl = "https://api.data.gov.in/resource/";
    }
    $form['ogd']['ogd_base_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('OGD API Base URL'),
      '#required' => TRUE,
      '#default_value' => $ogdBaseUrl,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('ogd.configsettings')
      ->set('ogd_api_key', $form_state->getValue('ogd_api_key'))
      ->set('ogd_base_url', $form_state->getValue('ogd_base_url'))
      ->save();
  }

}
