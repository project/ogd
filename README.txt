README.txt for Open Government Data Platform (OGD) India module
---------------------------

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration

INTRODUCTION
------------

This module focuses on the integration of Open Government Data
Public API with Drupal.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.

RECOMMENDED MODULES
-------------------

Markdown filter (https://www.drupal.org/project/markdown):
When enabled, display of the project's README.md help will be rendered
with markdown.

INSTALLATION
------------

 - Install the rtub module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
------------

* Configure the user permissions in Administration » People » Permissions:

This module procides following permission:
 - Administer OGD API Configuration

* Generate OGD API Key -
  Please refer following URL (https://data.gov.in/help/how-use-datasets-apis)

* Configure the OGD API Settings in /admin/config/ogd/adminsettings

This mpdules provides configuration form, where user store the OGD API key
and OGD API base url.

This module provides service, using this service any module will able to
access OGD API and get response in JSON format.

* You can get list of OGD API from following URL - https://data.gov.in

Example :

$service = \Drupal::service('ogd.ogd_request');
$resourseName = 'f001673e-60a7-4a51-9e27-3e001b7d9a19';
$param = [
        'offset' => 0,
        'limit' => 5,
        'filters' => array('district_code' => '02',),
      ];
$response = $service->request($resourseName, $param);

In above example resourse name will be the OGD api that you want to access.
